@file:Suppress("UnstableApiUsage")

import java.io.FileInputStream
import java.util.Properties

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-parcelize")
    id("kotlin-kapt")
    id("com.google.dagger.hilt.android")
    id("kotlin-android")
}

val properties = Properties()
properties.load(FileInputStream(rootProject.file("local.properties")))

android {
    namespace = "com.example.userdetailspagingapp"
    compileSdk = ConfigVersion.compileSdkVersion

    defaultConfig {
        applicationId = "com.example.userdetailspagingapp"
        minSdk = ConfigVersion.minSdkVersion
        targetSdk = ConfigVersion.targetSdkVersion
        versionCode = ConfigVersion.versionCode
        versionName = ConfigVersion.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        debug {
            isDebuggable = true
            isMinifyEnabled = false
            buildConfigField(
                "String",
                Keys.baseUrl,
                "\"" + properties["baseUrl"] + "\""
            )
        }

        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            buildConfigField(
                "String",
                Keys.baseUrl,
                "\"" + properties["baseUrl"] + "\""
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }

    buildFeatures {
        buildConfig = true
        viewBinding = true
        dataBinding = true
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = Versions.composeCompilerVersion
    }
    packagingOptions {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {

    implementation(Androidx.core)
    implementation(Androidx.appCompat)
    implementation(Google.materialDesign)
    implementation(Androidx.constraintlayout)
    implementation(Androidx.lifecycleRuntimeKtx)
    testImplementation(Testing.junit)
    androidTestImplementation(Testing.junitTest)
    androidTestImplementation(Testing.espressoCore)
    //navigation
    implementation(Androidx.navigationFragment)
    //Add viewmodel library
    implementation(Androidx.lifecycleViewModel)
    implementation(Androidx.lifecycleLiveData)
    //compose
    implementation(Compose.activityCompose)
    implementation(platform(Compose.composeBom))
    implementation(Compose.composeUi)
    implementation(Compose.composeUiGraphics)
    implementation(Compose.composeUiTooling)
    implementation(Compose.composeMaterial3)
    androidTestImplementation(platform(Compose.composeBom))
    androidTestImplementation(Testing.composeJunit4)
    debugImplementation(DebugImplementation.composeUiTooling)
    debugImplementation(DebugImplementation.composeUiTestManifest)
    // Coil Compose
    implementation(Compose.coilCompose)
    kapt(Androidx.lifecycleKapt)
    //Add coroutine library
    implementation(Jetbrains.coroutinesCore)
    testImplementation(Testing.coroutineTest)

    // retrofit
    implementation(Square.retrofit)
    implementation(Square.retrofitConverterGson)
    implementation(Square.retrofitOkhttpInerceptor)

    implementation(Jetbrains.kotlin)

    //hilt
    implementation(Dagger.daggerHilt)
    kapt(Dagger.daggerCompiler)

    //core:arch:testing
    testImplementation(Testing.androidArchCoreTesting)
    androidTestImplementation(Testing.androidArchCoreTesting)

    //mockito
    testImplementation(Testing.mockitoCore)
    testImplementation(Testing.mockitoKotlin)

    //turbine
    testImplementation(Testing.turbine)

    //mock webserver
    testImplementation(Testing.mockServerTest)
}