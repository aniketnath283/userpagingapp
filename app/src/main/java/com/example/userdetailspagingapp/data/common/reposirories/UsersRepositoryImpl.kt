package com.example.userdetailspagingapp.data.common.reposirories

import com.example.userdetailspagingapp.domain.entities.Users
import com.example.userdetailspagingapp.domain.repositories.UsersRemoteRepository
import com.example.userdetailspagingapp.domain.repositories.UsersRepository
import com.example.userdetailspagingapp.domain.utils.Resource
import javax.inject.Inject

class UsersRepositoryImpl @Inject constructor(private val usersRemoteRepository: UsersRemoteRepository): UsersRepository {
    override suspend fun getUsers(page: Int, limit: Int): Resource<Users> {
        return usersRemoteRepository.getUsers(page, limit)
    }
}