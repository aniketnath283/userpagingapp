package com.example.userdetailspagingapp.data.remote.repositories

import com.example.userdetailspagingapp.data.remote.apis.UserApi
import com.example.userdetailspagingapp.data.remote.utils.toUsers
import com.example.userdetailspagingapp.domain.entities.Users
import com.example.userdetailspagingapp.domain.repositories.UsersRemoteRepository
import com.example.userdetailspagingapp.domain.utils.Resource
import java.util.concurrent.CancellationException
import javax.inject.Inject

class UsersRemoteRepositoryImpl @Inject constructor(private val usersApi: UserApi): UsersRemoteRepository {
    override suspend fun getUsers(page: Int, limit: Int): Resource<Users> {
        return try {
            Resource.Success(usersApi.getUsers(page, limit).toUsers())
        } catch (e: Exception) {
            if (e is CancellationException) {
                throw e
            } else {
                e.printStackTrace()
                Resource.Error(e.message)
            }
        }
    }
}