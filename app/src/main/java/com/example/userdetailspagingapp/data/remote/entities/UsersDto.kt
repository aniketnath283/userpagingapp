package com.example.userdetailspagingapp.data.remote.entities

import com.google.gson.annotations.SerializedName

data class UsersDto(
    @SerializedName("data") var data: List<UserData> = listOf(),
    @SerializedName("total") var total: Int,
    @SerializedName("page") var page: Int,
    @SerializedName("limit") var limit: Int
)

data class UserData(
    @SerializedName("id") var id: String? = null,
    @SerializedName("title") var title: String? = null,
    @SerializedName("firstName") var firstName: String? = null,
    @SerializedName("lastName") var lastName: String? = null,
    @SerializedName("picture") var picture: String? = null
)
