package com.example.userdetailspagingapp.data.remote.utils

import com.example.userdetailspagingapp.data.remote.entities.UsersDto
import com.example.userdetailspagingapp.domain.entities.UserInfo
import com.example.userdetailspagingapp.domain.entities.Users

fun UsersDto.toUsersInfo(): List<UserInfo> {
    val usersInfoList: MutableList<UserInfo> = ArrayList()
    this.data.forEach { userData ->
        val userInfo = UserInfo(
            id = userData.id,
            title = userData.title,
            firstName = userData.firstName,
            lastName = userData.lastName,
            picture = userData.picture
        )
        usersInfoList.add(userInfo)
    }
    return usersInfoList
}

fun UsersDto.toUsers(): Users {
    val usersInfoList = toUsersInfo()
    return Users(
        data = usersInfoList,
        total = total,
        page = page,
        limit = limit
    )
}