package com.example.userdetailspagingapp.data.remote.utils

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()

        request.addHeader(ApiConstants.KEY_APP_ID, ApiConstants.VALUE_APP_ID)

        return chain.proceed(request.build())
    }
}