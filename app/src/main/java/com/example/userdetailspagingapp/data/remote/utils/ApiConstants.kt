package com.example.userdetailspagingapp.data.remote.utils

object ApiConstants {
    const val KEY_APP_ID = "app-id"
    const val VALUE_APP_ID = "65f59b57c0a557a11792fba0"
    const val V1_USERS = "v1/user"
}