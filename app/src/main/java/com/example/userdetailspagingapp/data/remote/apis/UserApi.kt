package com.example.userdetailspagingapp.data.remote.apis

import com.example.userdetailspagingapp.data.remote.entities.UsersDto
import com.example.userdetailspagingapp.data.remote.utils.ApiConstants
import retrofit2.http.GET
import retrofit2.http.Query

interface UserApi {

    @GET(ApiConstants.V1_USERS)
    suspend fun getUsers(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): UsersDto
}