package com.example.userdetailspagingapp.di

import com.example.userdetailspagingapp.data.remote.repositories.UsersRemoteRepositoryImpl
import com.example.userdetailspagingapp.domain.repositories.UsersRemoteRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class UsersRemoteRepositoryBinder {
    @Binds
    @Singleton
    abstract fun bindUsersRemoteRepository(usersRemoteRepositoryImpl: UsersRemoteRepositoryImpl): UsersRemoteRepository
}