package com.example.userdetailspagingapp.di

import com.example.userdetailspagingapp.data.common.reposirories.UsersRepositoryImpl
import com.example.userdetailspagingapp.domain.repositories.UsersRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class UsersRepositoryBinder {

    @Binds
    @Singleton
    abstract fun bindUsersRepository(usersRepositoryImpl: UsersRepositoryImpl): UsersRepository
}