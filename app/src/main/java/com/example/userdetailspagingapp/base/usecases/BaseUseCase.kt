package com.example.userdetailspagingapp.base.usecases

import kotlinx.coroutines.flow.StateFlow

interface BaseUseCaseWithAnyInput<in I, out O> {
    suspend fun process(input: I): O
}

interface BaseUseCasePagingSource<in I, out O> {
    suspend fun getDataFlow(): StateFlow<O>
    suspend fun process(input: I)
    suspend fun setOptInfo()
}