package com.example.userdetailspagingapp.base.entities


data class AppData<T>(
    var data: T ?= null,
    var isLoading: Boolean = false,
    var error: String ?= null,
    var isInitialLoad: Boolean = true
)
