package com.example.userdetailspagingapp.base.view

import android.os.Bundle
import androidx.compose.runtime.Composable
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.userdetailspagingapp.base.viewmodels.BaseViewModel
import com.example.userdetailspagingapp.ui.theme.UserdetailspagingappTheme
import kotlinx.coroutines.launch
import kotlin.reflect.KClass

abstract class BaseViewmodelComposeActivity<VM: BaseViewModel>(
    viewmodelClass: KClass<VM>
): BaseComposeActivity() {

    private val viewModel by lazy {
        ViewModelProvider(this, defaultViewModelProviderFactory)[viewmodelClass.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.observeViewModel()
            }
        }
    }

    @Composable
    override fun SetupViews() {
        UserdetailspagingappTheme {
            viewModel.SetupViews()
        }
    }

    @Composable
    open fun VM.SetupViews(){}

    open fun VM.observeViewModel() {}

    protected fun getViewmodel() = viewModel
}