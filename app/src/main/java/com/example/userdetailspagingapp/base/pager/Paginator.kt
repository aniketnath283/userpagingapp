package com.example.userdetailspagingapp.base.pager

interface Paginator<Key, Item> {
    suspend fun load(loadState: LoadStates)
    suspend fun refresh()
    fun setTotalPages(totalPages: Int)
    fun setMaxItems(maxItems: Int)
    fun getTotalPages(): Int
    fun getMaxItems(): Int
}