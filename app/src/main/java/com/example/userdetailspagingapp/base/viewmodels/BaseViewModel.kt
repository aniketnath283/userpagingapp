package com.example.userdetailspagingapp.base.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.userdetailspagingapp.base.usecases.BaseUseCasePagingSource
import com.example.userdetailspagingapp.base.usecases.BaseUseCaseWithAnyInput
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class BaseViewModel: ViewModel() {

    val dispatcher = Dispatchers.IO

    protected inline fun <I, O>fetchUseCaseWithAnyInput(
        baseUseCaseWithAnyInput: BaseUseCaseWithAnyInput<I, O>,
        input: I,
        crossinline response: (output: O) -> Unit
    ) {
        viewModelScope.launch(dispatcher) {
            response(baseUseCaseWithAnyInput.process(input))
        }
    }

    protected suspend fun <I,O>getBaseUseCaseDataFlow(
        baseUseCasePagingSource: BaseUseCasePagingSource<I, O>,
    ): StateFlow<O> {
        return withContext(viewModelScope.coroutineContext) {
            baseUseCasePagingSource.getDataFlow()
        }
    }

    protected fun <I,O>fetchBaseUseCasePagingSource(
        baseUseCasePagingSource: BaseUseCasePagingSource<I, O>,
        input: I
    ) {
        viewModelScope.launch(dispatcher) {
            baseUseCasePagingSource.process(input)
        }
    }
}