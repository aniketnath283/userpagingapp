package com.example.userdetailspagingapp.base.view

import android.os.Bundle
import android.widget.Toast
import com.example.userdetailspagingapp.base.viewmodels.BaseViewModel
import kotlin.reflect.KClass

abstract class BaseSessionViewModelComposeActivity<VM: BaseViewModel>(viewmodelClass: KClass<VM>)
    : BaseViewmodelComposeActivity<VM>(viewmodelClass) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewmodel().setupInitialApiCalls()
    }

    open fun VM.setupInitialApiCalls() {}

    open fun VM.takeActionOnError() {}

    protected fun showError(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }
}


