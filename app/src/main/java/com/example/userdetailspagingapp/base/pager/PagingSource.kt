package com.example.userdetailspagingapp.base.pager

import androidx.annotation.Keep
import com.example.userdetailspagingapp.domain.utils.Resource

open class PagingSource<Key, Item>(private val initialKey: Key,
                                   private inline val onLoadUpdated: suspend (Boolean) -> Unit,
                                   private inline val onRequest: suspend (nextKey: Key) -> Resource<List<Item>>,
                                   private inline val nextKey: suspend (currentKey: Key) -> Key,
                                   private inline val onError: suspend (Resource<List<Item>>) -> Unit,
                                   private inline val onSuccess: suspend (items: List<Item>?, newKey: Key) -> Unit):
    Paginator<Key, Item> {

    private var currentKey: Key = initialKey
    private var isMakingRequest = false
    private var keysSet: MutableSet<Key> = HashSet()
    private var allItems: MutableList<Item> = ArrayList()
    private var totalPages: Int = 0
    private var maxItems: Int = 0

    override suspend fun load(loadState: LoadStates) {
        if (LoadStates.REFRESH == loadState) {
            refresh()
            return
        }

        if (isMakingRequest) {
            return
        }

        isMakingRequest = true
        onLoadUpdated(true)
        val result = if (totalPages == 0 && maxItems == 0) {
            onRequest(currentKey)
        } else {
            if (totalPages > 0 && keysSet.size < totalPages) {
                onRequest(currentKey)
            } else if (maxItems > 0 && allItems.size < maxItems) {
                onRequest(currentKey)
            } else {
                Resource.Success(emptyList())
            }
        }

        isMakingRequest = false
        onLoadUpdated(false)

        result.data?.let {
            if (it.isNotEmpty()) {
                if (!keysSet.contains(currentKey)) {
                    allItems.addAll(it)
                    keysSet.add(currentKey)
                    currentKey = nextKey(currentKey)
                    onSuccess(allItems.toList(), currentKey)

                }
            }
        }?: kotlin.run {
            onError(result)
            return
        }
    }

    override suspend fun refresh() {
        currentKey = initialKey
        isMakingRequest = false
        keysSet.clear()
        allItems.clear()
        load(LoadStates.LOAD)
    }

    override fun setTotalPages(totalPages: Int) {
        if (this.totalPages == 0) {
            this.totalPages = totalPages
        }
    }

    override fun setMaxItems(maxItems: Int) {
        if (this.maxItems == 0) {
            this.maxItems = maxItems
        }
    }

    override fun getTotalPages(): Int {
        return totalPages
    }

    override fun getMaxItems(): Int {
        return maxItems
    }
}

@Keep
enum class LoadStates(state: Int) {
    REFRESH(0),
    LOAD(1)
}