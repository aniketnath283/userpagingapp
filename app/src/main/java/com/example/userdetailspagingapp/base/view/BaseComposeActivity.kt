package com.example.userdetailspagingapp.base.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import com.example.userdetailspagingapp.ui.theme.UserdetailspagingappTheme

abstract class BaseComposeActivity: ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            UserdetailspagingappTheme {
                SetupViews()
            }
        }
    }

    @Composable
    open fun SetupViews() {}
}
