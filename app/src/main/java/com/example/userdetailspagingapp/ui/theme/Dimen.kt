package com.example.userdetailspagingapp.ui.theme

import androidx.compose.ui.unit.dp

val dimen_4dp = 4.dp
val dimen_8dp = 8.dp
val dimen_64dp = 64.dp