package com.example.userdetailspagingapp.domain.usecases

import com.example.userdetailspagingapp.base.pager.LoadStates
import com.example.userdetailspagingapp.base.pager.PagingSource
import com.example.userdetailspagingapp.base.usecases.BaseUseCasePagingSource
import com.example.userdetailspagingapp.domain.entities.Users
import com.example.userdetailspagingapp.domain.utils.Resource
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class UsersPagingUseCase @Inject constructor(private val usersUseCase: UsersUseCase): BaseUseCasePagingSource<LoadStates, Resource<Users>> {
    private val usersData: MutableStateFlow<Resource<Users>> = MutableStateFlow(Resource.Loading(true))
    private var users = Users()
    private val pagingSource by lazy {
        PagingSource(
            initialKey = 1,
            onLoadUpdated = {
                usersData.emit(Resource.Loading(it))
            },
            onRequest = { nextPage ->
                val result = usersUseCase.process(LaunchParams(page = nextPage))
                result.data?.let {
                    users = it
                }
                Resource.Success(users.data)
            },
            nextKey = {
                users.page + 1
            },
            onError = {
                usersData.emit(Resource.Error(it.message))
            },
            onSuccess = { items, newKey ->
                users.apply {
                    items?.let {
                        data = items
                    }
                }
                usersData.emit(Resource.Success(users))
            }
        )
    }

    override suspend fun getDataFlow(): StateFlow<Resource<Users>> {
        return usersData
    }

    override suspend fun setOptInfo() {
        pagingSource.setMaxItems(users.total)
    }

    override suspend fun process(input: LoadStates) {
        setOptInfo()
        pagingSource.load(input)
    }
}