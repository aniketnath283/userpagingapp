package com.example.userdetailspagingapp.domain.utils

interface ExceptionsListener {
    fun unCaughtException(thread: Thread, throwable: Throwable)
}