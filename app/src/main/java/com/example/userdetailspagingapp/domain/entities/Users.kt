package com.example.userdetailspagingapp.domain.entities


data class Users(
    var data: List<UserInfo> = listOf(),
    var total: Int = 0,
    var page: Int = 0,
    var limit: Int = 20
)

data class UserInfo(
    var id: String? = null,
    var title: String? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var picture: String? = null
)
