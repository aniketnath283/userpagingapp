package com.example.userdetailspagingapp.domain.repositories

import com.example.userdetailspagingapp.domain.entities.Users
import com.example.userdetailspagingapp.domain.utils.Resource

interface UsersRemoteRepository {
    suspend fun getUsers(page: Int, limit: Int): Resource<Users>
}