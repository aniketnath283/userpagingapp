package com.example.userdetailspagingapp.domain.utils

sealed class Resource <T>(val data: T ?= null, val isLoading: Boolean = false, val message: String ?= null) {
    class Success<T>(data: T?): Resource<T>(data = data)
    class Loading<T>(loading: Boolean): Resource<T>(isLoading = loading)
    class Error<T>(message: String?): Resource<T>(message = message)
}
