package com.example.userdetailspagingapp.domain.usecases

import com.example.userdetailspagingapp.base.usecases.BaseUseCaseWithAnyInput
import com.example.userdetailspagingapp.domain.entities.Users
import com.example.userdetailspagingapp.domain.repositories.UsersRepository
import com.example.userdetailspagingapp.domain.utils.Resource
import javax.inject.Inject

class UsersUseCase @Inject constructor(private val usersRepository: UsersRepository): BaseUseCaseWithAnyInput<LaunchParams, Resource<Users>> {
    override suspend fun process(input: LaunchParams): Resource<Users> {
        return usersRepository.getUsers(input.page, input.limit)
    }
}

data class LaunchParams(
    val page: Int,
    val limit: Int = 20
)