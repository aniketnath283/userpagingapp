package com.example.userdetailspagingapp

import android.app.Application
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.example.userdetailspagingapp.domain.utils.ExceptionsListener
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class UsersApp: Application(), ExceptionsListener {

    override fun onCreate() {
        super.onCreate()
        setupExceptionHandler()
    }

    private fun setupExceptionHandler() {
        Handler(Looper.getMainLooper()).post {
            while (true) {
                try {
                    Looper.loop()
                } catch (e: Throwable) {
                    unCaughtException(Looper.getMainLooper().thread, e)
                }
            }
        }

        Thread.setDefaultUncaughtExceptionHandler { thread, error ->
            unCaughtException(thread = thread, throwable = error)
        }
    }

    override fun unCaughtException(thread: Thread, throwable: Throwable) {
        Log.d("ExceptionAnrReceived", "${throwable.printStackTrace()}")
    }
}