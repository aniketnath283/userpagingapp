package com.example.userdetailspagingapp.presentation.views.activities

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.userdetailspagingapp.base.view.BaseSessionViewModelComposeActivity
import com.example.userdetailspagingapp.presentation.views.compose.UserScreen
import com.example.userdetailspagingapp.presentation.viewsmodels.UsersViewModel
import com.example.userdetailspagingapp.ui.theme.white
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseSessionViewModelComposeActivity<UsersViewModel>(
    UsersViewModel::class
) {
    @Composable
    override fun UsersViewModel.SetupViews() {
        val usersState = this.userState
        usersState.error?.let {
            showError(it)
        }?: kotlin.run {
            Surface(modifier = Modifier.fillMaxSize(), color = white) {
                UserScreen(users = usersState) {
                    this.loadMore()
                }
            }
        }
    }

    override fun UsersViewModel.setupInitialApiCalls() {
        this.loadUsers()
        this.getUsersDataFlow()
    }
}
