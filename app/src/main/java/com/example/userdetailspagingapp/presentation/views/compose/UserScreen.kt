package com.example.userdetailspagingapp.presentation.views.compose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.userdetailspagingapp.base.entities.AppData
import com.example.userdetailspagingapp.domain.entities.Users
import com.example.userdetailspagingapp.ui.theme.dimen_8dp
import com.example.userdetailspagingapp.ui.theme.white

@Composable
fun UserScreen(
    users: AppData<Users>,
    load: () -> Unit
) {
    Box(modifier = Modifier.fillMaxSize()) {
        if (users.isLoading && users.isInitialLoad) {
            CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
        } else {
            LazyColumn(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
                users.data?.data?.let { usersList->
                    items(usersList.size) { position->
                        if (position == usersList.size - 1) {
                            load.invoke()
                        }
                        ItemUser(modifier = Modifier
                            .fillMaxSize()
                            .background(white), userInfo = usersList[position])
                    }
                    item {
                        if (users.isLoading) {
                            Row(modifier = Modifier
                                .fillMaxWidth()
                                .padding(dimen_8dp),
                                horizontalArrangement = Arrangement.Center) {
                                CircularProgressIndicator()
                            }
                        }
                    }
                }
            }
        }
    }
}