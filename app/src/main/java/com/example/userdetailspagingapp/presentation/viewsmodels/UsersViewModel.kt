package com.example.userdetailspagingapp.presentation.viewsmodels

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.viewModelScope
import com.example.userdetailspagingapp.base.entities.AppData
import com.example.userdetailspagingapp.base.pager.LoadStates
import com.example.userdetailspagingapp.base.viewmodels.BaseViewModel
import com.example.userdetailspagingapp.domain.entities.Users
import com.example.userdetailspagingapp.domain.usecases.UsersPagingUseCase
import com.example.userdetailspagingapp.domain.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(
    private val usersPagingUseCase: UsersPagingUseCase
): BaseViewModel() {

    var userState by mutableStateOf(AppData<Users>())

    fun loadUsers() {
        fetchBaseUseCasePagingSource(
            usersPagingUseCase,
            LoadStates.REFRESH
        )
        userState.isInitialLoad = true
    }

    fun getUsersDataFlow() {
        viewModelScope.launch {
            getBaseUseCaseDataFlow(usersPagingUseCase).collect { result ->
                when(result) {
                    is Resource.Success -> {
                        userState = userState.copy(
                            data = result.data,
                            error = null
                        )
                    }

                    is Resource.Error -> {
                        userState = userState.copy(
                            error = result.message
                        )
                    }

                    is Resource.Loading -> {
                        userState = userState.copy(
                            isLoading = result.isLoading
                        )
                    }
                }
            }
        }
    }

    fun loadMore() {
        fetchBaseUseCasePagingSource(
            usersPagingUseCase,
            LoadStates.LOAD
        )
        if (userState.isInitialLoad) {
            userState.isInitialLoad = false
        }
    }
}