package com.example.userdetailspagingapp.presentation.views.compose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import coil.compose.AsyncImage
import com.example.userdetailspagingapp.domain.entities.UserInfo
import com.example.userdetailspagingapp.ui.theme.black
import com.example.userdetailspagingapp.ui.theme.dimen_4dp
import com.example.userdetailspagingapp.ui.theme.dimen_64dp
import com.example.userdetailspagingapp.ui.theme.white

@Composable
fun ItemUser(
    modifier: Modifier,
    userInfo: UserInfo
) {
    Spacer(modifier = Modifier.height(dimen_4dp))
    Card(modifier = modifier, elevation = CardDefaults.cardElevation(dimen_4dp)) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(white)
                .height(IntrinsicSize.Max)
                .padding(dimen_4dp)
        ) {
            AsyncImage(
                model = userInfo.picture,
                contentDescription = "User Image",
                modifier = Modifier
                    .weight(1f)
                    .height(dimen_64dp)
            )
            Spacer(modifier = Modifier.width(dimen_4dp))
            Column(
                modifier = Modifier
                    .weight(5f)
                    .fillMaxHeight()
            ) {
                userInfo.title?.let {
                    Text(
                        text = it,
                        style = MaterialTheme.typography.headlineSmall,
                        color = black,
                        modifier = Modifier.fillMaxWidth()
                    )
                }
                Spacer(modifier = Modifier.height(dimen_4dp))
                val name = "${userInfo.firstName} ${userInfo.lastName}"
                Text(text = name, color = black, modifier = Modifier.fillMaxWidth())
            }
        }
    }
}

@Composable
@Preview
fun PreviewItemUser() {
    ItemUser(
        modifier = Modifier
            .fillMaxSize()
            .background(white), userInfo = UserInfo(
            id = "xjkfnvxv",
            title = "Mr.",
            firstName = "Aniket",
            lastName = "Nath",
            picture = "https://randomuser.me/api/portraits/med/men/67.jpg"
        )
    )
}